#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
from textblob import TextBlob


# In[2]:


## reading data
data = pd.read_csv("Downloads/review_data.csv")


# In[3]:


data.head(20)


# In[4]:


data.shape


# In[5]:


data.info()


# In[6]:


## total there are 3k reviews . In this all we need is text column and star (rating ) and one null value in text


# In[7]:


data.isnull().sum()


# In[ ]:





# In[8]:


data.dropna(axis = 'index', how ='any', inplace = True)
  


# In[9]:


data.isnull().sum()


# In[10]:


sns.distplot(data["star"])


# In[ ]:





# In[ ]:





# In[11]:


data['text'][500]


# In[12]:


text_blob_object=TextBlob(data['text'][500])
print(text_blob_object.sentiment)


# In[13]:


def get_pol(review):
    return TextBlob(review).sentiment.polarity
data['sentiment_polarity']=data['text'].apply(get_pol)
data.head(20)


# In[14]:


data.head()


# In[15]:


data.shape


# In[16]:


data['sentiment_polarity']


# In[ ]:





# In[17]:


sns.distplot(data['sentiment_polarity'])


# In[18]:


sns.barplot(x='star',y='sentiment_polarity',data=data)


# In[19]:


most_neg = data[data.sentiment_polarity== -1].text
print(most_neg)


# In[20]:


negative_reviews = data[data.sentiment_polarity<0].text
print(negative_reviews)


# In[21]:


most_posit=data[data.sentiment_polarity==1].text
print(most_posit)


# In[25]:


positive_reviews=data[data.sentiment_polarity>0].text
print(positive_reviews)



# In[ ]:


## our required data


# In[22]:


badstar_goodreview= data.loc[(data['star'] ==1) &
              data['sentiment_polarity']==1]
print(badstar_goodreview)


# In[32]:


req_data=pd.DataFrame(data.loc[(data['star'] ==1) &
              data['sentiment_polarity']==1])


# In[33]:


req_data


# In[ ]:




