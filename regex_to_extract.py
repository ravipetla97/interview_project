#!/usr/bin/env python
# coding: utf-8

# In[ ]:


##if we consider the given data as a string


# In[12]:


import re


# In[13]:


dataex=' {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}'



# In[14]:


re.findall(r'\d+', dataex)


# In[ ]:


##if we consider as given in question as DICTIONARIES inside SETs and again the whole thing inside a DICTIONARY


# In[2]:


data = {"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}


# In[3]:


data


# In[4]:


## this is looking like complex data::: DICTIONARIES inside SETs and again the whole thing inside a DICTIONARY


# In[36]:


len(data)


# In[37]:


## iterating SETS INSIDE DICTIONARIES


# In[38]:


for i in data.keys():
    for j in data[i]:
        print (j)
        


# In[39]:


## iterating SETS INSIDE DICTIONARIES AND KEY PAIR VALUES INSIDE THE SETS


# In[40]:


for i in data.keys():
    for j in data[i]:
        for k in j.keys():
            print(j[k])
        


# In[41]:


## BUT WE HAVE TO GET ONLY THE NUMERICAL VALUES


# In[42]:


for i in data.keys():
    for j in data[i]:
        for k in j.keys():
            if (type(j[k])==int or type(j[k])==float):
                print(j[k])
           
        


# In[43]:


##extracted all the numerical values


# In[ ]:




